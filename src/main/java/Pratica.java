
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import utfpr.ct.dainf.pratica.Lancamento;
import utfpr.ct.dainf.pratica.ProcessaLancamentos;

/**
 * IF62C Fundamentos de Programação 2
 * Avaliação parcial.
 * @author 
 */
public class Pratica {
 
    public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
        Scanner scanner = new Scanner(System.in);
        String caminho = "";
        Integer num;
        System.out.println("Digite o caminho completo para o arquivo de lançamentos: ");
        if(scanner.hasNextLine()){
            caminho = scanner.nextLine();
        }
        ProcessaLancamentos l = new ProcessaLancamentos(caminho);
        List<Lancamento> lista = l.getLancamentos();
        System.out.println("Digite números de conta, digite 0 para sair."); 
        do{
            while(true){
                System.out.println("Digite o número da conta: ");
                String input = scanner.next();
                try{
                    num = Integer.parseInt(input);
                    break;
                } catch(NumberFormatException e){
                    System.out.println("Por favor, informe um valor numérico ");
                }
            }
            if(num == 0){
                break;
            }
            exibeLancamentosConta(lista, num);
        } while(true);
        scanner.close();
    }
    
    public static void exibeLancamentosConta(List<Lancamento> lancamentos, Integer conta) {
        int cont = 0;
        for(Lancamento l : lancamentos){
            if(l.getConta().equals(conta)){
                System.out.println(l.toString());
                cont++;
            }            
        }
        if(cont == 0){
            System.out.println("Conta inexistente");
        }
    }
 
}