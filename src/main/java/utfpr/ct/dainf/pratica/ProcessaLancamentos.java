package utfpr.ct.dainf.pratica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Spliterator;

/**
 * Linguagem Java
 * @author
 */
public class ProcessaLancamentos {
    private BufferedReader reader;

    public ProcessaLancamentos(File arquivo) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(arquivo));
    }

    public ProcessaLancamentos(String path) throws FileNotFoundException {
        reader = new BufferedReader(new FileReader(new File(path)));
    }
    
    private String getNextLine() throws IOException {
        return reader.readLine();
    }
    
    private Lancamento processaLinha(String linha) throws ParseException {
        Integer conta = new Integer(linha.substring(0, 6));
        DateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        Date data = fmt.parse(linha.substring(6, 14));
        String desc = linha.substring(14, 74).trim();
        Double valor = new Double(linha.substring(74, 86))/100;
        return new Lancamento(conta, data, desc, valor);
    }
    
    private Lancamento getNextLancamento() throws IOException, ParseException {
        String linha;
        while((linha = getNextLine()) != null){
            return processaLinha(linha);
        }
        return null;
    }
    
    public List<Lancamento> getLancamentos() throws IOException, ParseException {
        List<Lancamento> lista = new ArrayList<>();
        Lancamento l;
        while((l = getNextLancamento()) != null){
            lista.add(l);
        }
        reader.close();
        return lista;
    }
    
}
