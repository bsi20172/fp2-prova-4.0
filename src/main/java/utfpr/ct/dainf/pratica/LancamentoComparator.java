package utfpr.ct.dainf.pratica;

import java.util.Comparator;

/**
 * Linguagem Java
 * @author
 */
public class LancamentoComparator implements Comparator<Lancamento>{
    
    @Override
    public int compare(Lancamento t, Lancamento t1) {
        final int comparacaoConta = t.getConta().compareTo(t1.getConta());
        if(comparacaoConta != 0){
            return comparacaoConta;
        }
        return t1.getData().compareTo(t1.getData());
    }

 
    
}
